import axios from 'axios'
const instance = axios.create({
  baseURL: 'http://localhost:3000'
})

function delay(sec: number) {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(sec), sec)
  })
}

instance.interceptors.response.use(
  async function (res) {
    await delay(3)
    // console.log(res)
    return res
  },
  function (error) {
    return Promise.reject(error)
  }
)

export default instance
