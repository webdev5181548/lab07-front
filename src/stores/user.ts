import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import userService from '@/services/user'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
  const loadingStore = useLoadingStore()
  const users = ref<User[]>([])

  async function getUsers() {
    loadingStore.doLoading()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStore.finish()
  }

  async function getUser(id: number) {
    loadingStore.doLoading()
    const res = await userService.getUser(id)
    users.value = res.data
    loadingStore.finish()
  }

  async function saveUser(user: User) {
    loadingStore.doLoading()
    if (user.id < 0) {
      // add new
      const res = await userService.addUser(user)
    } else {
      // update
      const res = await userService.updateUser(user)
    }
    await getUsers()
    loadingStore.finish()
  }

  async function deleteUser(user: User) {
    loadingStore.doLoading()
    const res = await userService.delUser(user)
    await getUsers()
    loadingStore.finish()
  }

  return { users, getUsers, saveUser, deleteUser, getUser }
})
